import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Image.asset("assets/Header.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text("تسجيل حساب جديد",
            style: TextStyle(
              color: Color(0xff128D58),
            )),
      ),
      body: ListView(
        children: [
          Container(
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 150,
                    ),
                    Text(
                      "تسجيل حساب جديد",
                      style: TextStyle(
                          fontSize: 30,
                          color: Color(0xff140D43),
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      hintText: '   رقم التليفون-2+',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      hintText: '   الاسم الاول',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    cursorColor: Color(0xff128D58),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      hintText: 'اسم العائلة',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    cursorColor: Color(0xff128D58),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      hintText: '   البريد الالكتروني',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    cursorColor: Color(0xff128D58),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      hintText: '   كلمه المرور',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 150,
                ),
                FractionallySizedBox(
                  child: Text(
                    "بتسجيل الحساب لقد قمت بالموافقه\nعلي شروط و الاحكام الخاصه بالخدمه",
                    style: TextStyle(),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      "تسجيل الحساب",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.bold),
                    ),
                    color: Color(0xff128D58),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
