import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ma3aaak/UI/home_screen.dart';

class DetailsOfRequests extends StatefulWidget {
  @override
  _DetailsOfRequestsState createState() => _DetailsOfRequestsState();
}

class _DetailsOfRequestsState extends State<DetailsOfRequests> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "تفاصيل الطلبات",
          style: TextStyle(
              fontSize: 25,
              color: Color(0xff2C2E6F),
              fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Image.asset("assets/Group 74.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(),
                Text(
                  "هنا تجد تفاصيل الطلب و امكانيه رفضه او قبوله",
                  style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff128D58),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Expanded(child: ListView(
              children: [
                
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 20.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 43, 0),
                    child: Text(
                      "12 مايو , 2020",
                      style: TextStyle(
                          color: Color(0xff2C2E6F),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":التاريخ",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 30.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                    child: Text(
                      "10002020",
                      style: TextStyle(
                          color: Color(0xff2C2E6F),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":رقم الطلب",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 25.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 60, 0),
                    child: Text(
                      "52",
                      style: TextStyle(
                          color: Color(0xff2C2E6F),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":السن",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 15.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                    child: Text(
                      "تركيب محلول وريدي لسيده في الخمسين من عمرها\n                                  تركيب محلول وريدي لسيده",
                      style: TextStyle(
                          color: Color(0xffB1BAC8),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":نوع الخدمة",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 20.0, 20.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                      icon: Image.asset("assets/Path.png"), onPressed: null),
                  Padding(
                    padding: const EdgeInsets.only(right: 60.0),
                    child: IconButton(
                        icon: Image.asset("assets/telephone.png"),
                        onPressed: null),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 22, 0),
                    child: Text(
                      "01033072555",
                      style: TextStyle(
                          color: Color(0xff2C2E6F),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":رقم الهاتف",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 20.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 50, 0),
                    child: Text(
                      "37 شارع مصدق , الدقي ",
                      style: TextStyle(
                          color: Color(0xff2C2E6F),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":العنوان",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 20.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 25, 0),
                    child: Text(
                      "كاش ",
                      style: TextStyle(
                          color: Color(0xff2C2E6F),
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ":نوع الدفع",
                    style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20.0, 20.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
                    child: Container(
                      height: 30,
                      width: 230,
                      child: Card(
                        elevation: 5,
                        child: TextField(
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.left,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            fillColor: Colors.white,
                            filled: true,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(
                      ":كود العميل",
                      style: TextStyle(
                          color: Color(0xff128D58),
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
             Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 20.0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    children: [
                      Row(
                        children: [
                          IconButton(
                              icon: Image.asset("assets/star.png"),
                              onPressed: null),
                          IconButton(
                              icon: Image.asset("assets/star.png"),
                              onPressed: null),
                          IconButton(
                              icon: Image.asset("assets/star.png"),
                              onPressed: null),
                          IconButton(
                              icon: Image.asset("assets/star.png"),
                              onPressed: null),
                          IconButton(
                              icon: Image.asset("assets/star.png"),
                              onPressed: null),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
                        child: Container(
                          height: 67,
                          width: 230,
                          child: Card(
                            elevation: 5,
                            child: TextField(
                              keyboardType: TextInputType.text,
                              textAlign: TextAlign.left,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                fillColor: Colors.white,
                                filled: true,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 60),
                    child: Text(
                      ":تقييم العملية",
                      style: TextStyle(
                          color: Color(0xff128D58),
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            FractionallySizedBox(
              widthFactor: 0.8,
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Text(
                    "انهاء",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                  color: Color(0xff128D58),
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) => HomeScreen()));
                  }),
            ),
              ],
            ))
                    ],
        ),
      ),
    );
  }
}
