import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/login_page.dart';
import 'package:ma3aaak/UI/register.dart';

class ProfilePesronly extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 100.0),
            child: Center(
              child: Image.asset(
                "assets/Hero Image.png",
                alignment: Alignment.bottomCenter,
              ),
            ),
          ),
          SizedBox(
            height: 90,
          ),
          Container(
            child: Column(
              children: [
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      "تسجيل حساب جديد",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    color: Color(0xff128D58),
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => LoginPage()

                          )
                          );
                    },
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                FractionallySizedBox(
                  alignment: Alignment.center,
                  widthFactor: 0.9,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                        color: Color(0xff128D58),
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    color: Colors.white,
                    onPressed: () {
                       Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Register()));
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 100.0),
                child: Container(child: Image.asset("assets/person2.png")),
              ),
              Image.asset("assets/logo.png"),
            ],
          )),
    );
  }
}
