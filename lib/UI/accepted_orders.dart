import 'package:calendar_timeline/calendar_timeline.dart';
import 'package:flutter/material.dart';
import 'package:ma3aaak/models/requet_model.dart';
import 'package:ma3aaak/widget/new_acceptedorders_item.dart';

class AcceptedOrders extends StatefulWidget {
  //المواعيد القادمه
  @override
  _AcceptedOrdersState createState() => _AcceptedOrdersState();
}

class _AcceptedOrdersState extends State<AcceptedOrders> {
  List<AcceptedModel> acceptedList = [
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
    AcceptedModel(
      orderDay: '17',
      orderMonth: 'ابريل',
      orderSubject: 'تغيير كلونة و تركيب محلول ',
      orderTime: 'من 1 صباحا الي 12 مساءا',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(),
                Text(
                  "هنا تجدي جميع الطلبات التي تم قبولها",
                  style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff128D58),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            CalendarTimeline(
              initialDate: DateTime(2020, 4, 20),
              firstDate: DateTime(2019, 1, 15),
              lastDate: DateTime(2020, 11, 20),
              onDateSelected: (date) => print(date),
              leftMargin: 20,
              monthColor: Color(0xff128D58),
              dayColor: Colors.black,
              activeDayColor: Colors.white,
              activeBackgroundDayColor: Color(0xff128D58),
              dotsColor: Colors.white,
              selectableDayPredicate: (date) => date.day != 23,
              locale: 'en_ISO',
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                  itemBuilder: (ctx, index) {
                    return NewAccepttedordersItem(model3: acceptedList[index]);
                  },
                  itemCount: acceptedList.length,
                ),
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "المواعيد القادمه",
          style: TextStyle(
              fontSize: 25,
              color: Color(0xff2C2E6F),
              fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Image.asset("assets/Group 74.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
    );
  }
}
