import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/home.dart';
import 'package:ma3aaak/UI/profile_personly.dart';

class HomeScreen extends StatefulWidget {
  static final routeName = '/main_screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _pageIndex = 0;
  PageController _pageController;

  List<Widget> tabPages = [
    Home(),
    ProfilePesronly(),
  ];

  var pages =[{
    'page':Home(),'title':'Home'
  },{
    'page':ProfilePesronly(),'title':'Profile'
  }];
  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _pageIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _pageIndex,
        onTap: onPageChanged,
        fixedColor: Color(0xff128D58),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: new Text(pages[0]['title']),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: new Text(pages[1]['title']),
          ),
        ],
      ),
      body: 
       
       
      pages[_pageIndex]['page'],
    );
  }

  void onPageChanged(int page) {
    setState(() {
      this._pageIndex = page;
    });
  }

 
}
