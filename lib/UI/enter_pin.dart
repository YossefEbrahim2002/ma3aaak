import 'package:flutter/material.dart';
import 'package:sms_autofill/sms_autofill.dart';

class EnterPin extends StatefulWidget {
  @override
  _EnterPinState createState() => _EnterPinState();
}

class _EnterPinState extends State<EnterPin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset("assets/Group 74.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Text(
                        " ادخل الاربع ارقام التي\n            تم ارسالها الي",
                        style: TextStyle(
                            fontSize: 30,
                            color: Color(0xff140D43),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Text(
                        "01001124447",
                        style: TextStyle(
                            fontSize: 30,
                            color: Color(0xff128D58),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 50,
                    vertical: 10,
                  ),
                  child: PinFieldAutoFill(
                    codeLength: 4,
                    
                    
                  ),
                ),
                SizedBox(
                  height: 110,
                ),
                FractionallySizedBox(
                  child:Text(
                    "!لم استلم الرمز",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color(0xff128D58),
                    ),
                  ),
                 
                ),
                
              ],
            ),
          ],
        ),
      ),
    );
  }
}
