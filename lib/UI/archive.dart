import 'package:flutter/material.dart';
import 'package:ma3aaak/models/requet_model.dart';
import 'package:ma3aaak/widget/new_archive_item.dart';

class Archive extends StatefulWidget {
  //سجل الانشطه
  @override
  _ArchiveState createState() => _ArchiveState();
}

class _ArchiveState extends State<Archive> {
  List<ArchiveModel> requestList = [
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    ArchiveModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "سجل الانشطه",
          style: TextStyle(
              fontSize: 25,
              color: Color(0xff2C2E6F),
              fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Image.asset("assets/Group 74.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    "هنا جميع الأنشطة و الطلبات التي تم اداؤها ",
                    style: TextStyle(
                        fontSize: 20,
                        color: Color(0xff128D58),
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: ListView.builder(
                  itemBuilder: (ctx, index) {
                    return NewArchiveItem(model2: requestList[index]);
                  },
                  itemCount: requestList.length,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
