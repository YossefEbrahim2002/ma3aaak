import 'package:flutter/material.dart';

class Earning extends StatefulWidget {
  //الارباح
  @override
  _EarningState createState() => _EarningState();
}

class _EarningState extends State<Earning> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "الارباح",
          style: TextStyle(
              color: Color(0xff2C2E6F),
              fontSize: 25,
              fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Image.asset("assets/Group 74.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(
                    "هنا جميع الأرباح التي تم الحصول عليها ",
                    style: TextStyle(
                        fontSize: 20,
                        color: Color(0xff128D58),
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            FractionallySizedBox(
              child: Text(
                "الأرباح المجمعة",
                style: TextStyle(
                  fontSize: 33,
                  color: Color(0xff128D58),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: 200,
              height: 60,
              alignment: Alignment.center,
              child: Row(
                children: [
                  SizedBox(),
                  Text(
                    "1250",
                    style: TextStyle(
                      fontSize: 53,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff2C2E6F),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Color(0xff2C2E6F), spreadRadius: 1),
                      ],
                    ),
                    width: 55.35,
                    height: 28.65,
                    child: Text(
                      "جنيه",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 21,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff2C2E6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              color: Colors.grey[300],
              width: 400,
              height: 1,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "المديونية للشركة",
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xffB1BAC8),
                      fontWeight: FontWeight.bold),
                ),
                Container(
                  width: 131,
                  height: 37,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Color(0xff707070), spreadRadius: 1),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      "إجمالي التحصيل",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 17,
                          color: Color(0xff128D58),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
height: 30,
            ),
            Container(
              width: 200,
              height: 60,
              alignment: Alignment.center,
              child: Row(
                children: [
                  SizedBox(),
                  Text(
                    "2000",
                    style: TextStyle(
                      fontSize: 53,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff2C2E6F),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Color(0xff2C2E6F), spreadRadius: 1),
                      ],
                    ),
                    width: 55.35,
                    height: 28.65,
                    child: Text(
                      "جنيه",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 21,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff2C2E6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              width: 400,
              height: 1,
              color: Colors.grey[300],
            ),
          ],
        ),
      ),
    );
  }
}
