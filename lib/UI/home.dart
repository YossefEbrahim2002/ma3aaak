import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/accepted_orders.dart';
import 'package:ma3aaak/UI/archive.dart';
import 'package:ma3aaak/UI/earning.dart';
import 'package:ma3aaak/UI/pending_orders.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: new ListView(
        children: [
          new Container(
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: new Column(
                    children: [new Image.asset("assets/Text.png")],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      RaisedButton(
                        //الطلبات الجديده
                        elevation: 0,
                        color: Colors.white,
                        child: new Image.asset(
                          "assets/Group 1361.png",
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PendingOrders()));
                        },
                      ),
                      RaisedButton(
                        //المواعيد القادمه

                        elevation: 0,
                        color: Colors.white,
                        child: new Image.asset(
                          "assets/Group 1363.png",
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AcceptedOrders()));
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      RaisedButton(
                        //الارباح
                        elevation: 0,
                        color: Colors.white,
                        child: new Image.asset(
                          "assets/Group 1364.png",
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Earning()));
                        },
                      ),
                      RaisedButton(
                        //سجل الانشطه
                        elevation: 0,
                        color: Colors.white,
                        child: new Image.asset(
                          "assets/Group 1365.png",
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Archive()));
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 34.0),
                  child: new Container(
                    alignment: Alignment.topRight,
                    child: new Text(
                      "الانشطه القادمه",
                      style: TextStyle(
                        fontSize: 35,
                        color: Color(0xff2C2E6F),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              height: 200,
              width: 150,
              child: new ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  new Row(
                    children: [
                      new Container(
                        alignment: Alignment.topRight,
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 180.0),
                              child: new Container(
                                alignment: Alignment.bottomRight,
                                child: new Text(
                                  "مايو ,12 2020 ",
                                  style: TextStyle(
                                    color: Color(0xff2C2E6F),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 50.0),
                              child: new Container(
                                child: new Text(
                                  "تركيب محلول وريدي",
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: Color(0xff2C2E6F),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: new Container(
                                child: new Text(
                                  "برجاء تركيب محلول وريدي لسيده في الخمسين من\n                عمرهابرجاء تركيب محلول وريدي لسيده",
                                  style: TextStyle(
                                    color: Color(0xffB1BAC8),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 80.0),
                              child: new Container(
                                child: new Text(
                                  "العنوان:37 جابر بن حيان الدقي",
                                  style: TextStyle(
                                    color: Color(0xff128D58),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        alignment: Alignment.topRight,
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 180.0),
                              child: new Container(
                                alignment: Alignment.bottomRight,
                                child: new Text(
                                  "مايو ,12 2020 ",
                                  style: TextStyle(
                                    color: Color(0xff2C2E6F),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 50.0),
                              child: new Container(
                                child: new Text(
                                  "تركيب محلول وريدي",
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: Color(0xff2C2E6F),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: new Container(
                                child: new Text(
                                  "برجاء تركيب محلول وريدي لسيده في الخمسين من\n                عمرهابرجاء تركيب محلول وريدي لسيده",
                                  style: TextStyle(
                                    color: Color(0xffB1BAC8),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 80.0),
                              child: new Container(
                                child: new Text(
                                  "العنوان:37 جابر بن حيان الدقي",
                                  style: TextStyle(
                                    color: Color(0xff128D58),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Image.asset("assets/notfecation.png"),
              Text(
                "الصفحه الرئيسيه",
                style: TextStyle(
                    fontSize: 25,
                    color: Color(0xff2C2E6F),
                    fontWeight: FontWeight.bold),
              ),
              Image.asset("assets/logo.png"),
            ],
          )),
    );
  }
}
