import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/forgot_password.dart';

class Register extends StatefulWidget {
  @override
  _Register createState() => _Register();
}

class _Register extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Image.asset("assets/Header.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text("تسجيل الدخول",
            style: TextStyle(
              color: Color(0xff128D58),
            )),
      ),
      body: ListView(
        children: [
          Container(
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 200,
                    ),
                    Text(
                      "تسجيل الدخول ",
                      style: TextStyle(
                          fontSize: 30,
                          color: Color(0xff140D43),
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    cursorColor: Color(0xff128D58),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      hintText: 'رقم التليفون او البريد الالكتروني',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                FractionallySizedBox(
                  widthFactor: 0.9,
                  child: TextField(
                    textAlign: TextAlign.right,
                    cursorColor: Color(0xff128D58),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      hintText: '    كلمه المرور',
                      labelStyle: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 80,
                ),
                FractionallySizedBox(
                  child:RaisedButton(
                    elevation: 0,
                    color: Colors.white,
                   child:Text(
                    "نسيت كلمه المرور ؟",
                    style: TextStyle(),
                  ),
                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) =>ForgotPassword()
                    )
                    );
                  },
                ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    ),
                    color: Color(0xff128D58),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
