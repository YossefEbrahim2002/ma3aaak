import 'package:flutter/material.dart';
import 'package:ma3aaak/models/requet_model.dart';
import 'package:ma3aaak/widget/new_request_item.dart';

class PendingOrders extends StatefulWidget {
  //الطلبات الجديده
  @override
  _PendingOrdersState createState() => _PendingOrdersState();
}

class _PendingOrdersState extends State<PendingOrders> {
  List<RequestModel> requestList = [
    RequestModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    RequestModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    RequestModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
   
    RequestModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),
    RequestModel(
      orderNumber: '10002020',
      orderDate: '12 مايو , 2020',
      orderAdress: '37 شارع مصدق , الدقي ',
      orderStatus:
          'برجاء تركيب محلول وريدي لسيدة في الخمسين من عمرها\n                                   برجاء تركيب محلول وريدي لسيدة.',
    ),

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "الطلبات الجديده",
          style: TextStyle(
              fontSize: 25,
              color: Color(0xff2C2E6F),
              fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Image.asset("assets/Group 74.png"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(),
                Text(
                  "الطلبات المقبولة تجدها في المواعيد القادمة",
                  style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff128D58),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: ListView.builder(
                  itemBuilder: (ctx, index) {
                    return NewRequestItem(model: requestList[index]);
                  },
                  itemCount: requestList.length,
                ),
              ),
            ),
          ], //childmain
        ),
      ),
    );
  }
}
