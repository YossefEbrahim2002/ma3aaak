import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/home_screen.dart';
import 'package:ma3aaak/UI/splash_screen.dart';

void main() {
  runApp(new MaterialApp(
    theme: ThemeData(primaryColor: Color(0xff128D58),accentColor: Color(0xff128D58)),
    home:new SplashScreen() ,
    routes: {
      HomeScreen.routeName : (context) => HomeScreen(),
    },
  )
  );
}

