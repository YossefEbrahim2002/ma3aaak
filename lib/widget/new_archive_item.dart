import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/details_of_requests.dart';
import 'package:ma3aaak/models/requet_model.dart';

class NewArchiveItem extends StatelessWidget {
  final ArchiveModel model2;

  const NewArchiveItem({Key key, @required this.model2}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DetailsOfRequests()));
      },
      child: Card(
        elevation: 10,
        child: Container(
          width: 325,
          height: 175,
          child: Stack(
            children: <Widget>[
              Positioned(
                left: 0,
                top: 0,
                bottom: 0,
                width: 100,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left:10),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                            child: Text(
                              "تم بنجاح",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          color: Color(0xff128D58),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                ), // replace with your image
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50.0, 2.0, 2.2, 3.2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(140.0, 0, 0, 0),
                      child: Container(
                        // التاريخ
                        color: Colors.white,
                        height: 20,
                        width: 130,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Text(
                                model2.orderDate,
                                style: TextStyle(
                                    color: Color(0xff2C2E6F),
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,0,8,0),
                              child: Text(
                                ":التاريخ ",
                                style: TextStyle(
                                    color: Color(0xff128D58),
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(120.0, 0, 0, 0),
                      child: Container(
                        // العنوان
                        color: Colors.white,
                        height: 20,
                        width: 155,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Text(
                                model2.orderAdress,
                                style: TextStyle(
                                    color: Color(0xff2C2E6F),
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 6.0),
                              child: Text(
                                ":العنوان  ",
                                style: TextStyle(
                                    color: Color(0xff128D58),
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      // الحاله
                      color: Colors.white,
                      height: 42,
                      width: 270,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              model2.orderStatus,
                              style: TextStyle(
                                  color: Color(0xffB1BAC8),
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0,0,10,0),
                            child: Text(
                              ":الحالة",
                              style: TextStyle(
                                  color: Color(0xff128D58),
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
