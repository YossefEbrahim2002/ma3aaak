import 'package:flutter/material.dart';
import 'package:ma3aaak/UI/details_of_requests.dart';
import 'package:ma3aaak/models/requet_model.dart';

class NewAccepttedordersItem extends StatelessWidget {
  final AcceptedModel model3;

  const NewAccepttedordersItem({Key key, @required this.model3})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DetailsOfRequests()));
      },
      child: FractionallySizedBox(
        widthFactor: 0.8,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Color(0xff128D58), spreadRadius: 1),
                    ],
                  ),
                  height: 75,
                  width: 235,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        model3.orderSubject,
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xff2C2E6F),
                        ),
                      ),
                      Text(model3.orderTime),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Column(
                  children: [
                    Text(
                      model3.orderMonth,
                      style: TextStyle(
                        fontSize: 14,
                        color: Color(0xff2C2E6F),
                      ),
                    ),
                    Text(
                      model3.orderDay,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Color(0xff2C2E6F),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
