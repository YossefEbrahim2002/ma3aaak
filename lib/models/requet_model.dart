class RequestModel{
  final String orderNumber;
  final String orderDate;
  final String orderAdress;
  final String orderStatus;

  RequestModel({this.orderNumber, this.orderDate, this.orderAdress, this.orderStatus});
}
class ArchiveModel{
  final String orderNumber;
  final String orderDate;
  final String orderAdress;
  final String orderStatus;

  ArchiveModel({this.orderNumber, this.orderDate, this.orderAdress, this.orderStatus});
}
class AcceptedModel{
  final String orderDay;
  final String orderMonth;
  final String orderTime;
  final String orderSubject;

  AcceptedModel({this.orderDay, this.orderMonth, this.orderTime, this.orderSubject});
}